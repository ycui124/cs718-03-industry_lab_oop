package ictgradschool.industry.lab_oop.ex01;

public class ExerciseOne {
    public static void main(String[] args) {
        //After the following statements are executed, what are the values stored in each variable?
        int a = 7;
        int b = 1;
        int c = a + 2;
        a = b;
        b = c;
        c = c + 1;
        //a=1,b=9,c=10
        System.out.println(a+" "+b+" "+c);


        //After the following statements are executed, what are the outputs?
        // The method setFruitName(String) changes the fruit name and the method getFruitName()
        // returns the fruit name of the object.
        /*Fruit apple = new Fruit("red apple");
        Fruit orange = new Fruit("orange");
        Fruit greenApple = apple;

        //red apple
        System.out.println("The fruit is " + apple.getFruitName());
        //orange
        System.out.println("The fruit is " + orange.getFruitName());
        //red apple
        System.out.println("The fruit is " + greenApple.getFruitName());
        orange.setFruitName("navel orange");
        greenApple.setFruitName("green apple");
        //red apple
        System.out.println("The fruit is " + apple.getFruitName());
        //navel orange
        System.out.println("The fruit is " + orange.getFruitName());
        //green apple
        System.out.println("The fruit is " + greenApple.getFruitName());
         */

    }
}
