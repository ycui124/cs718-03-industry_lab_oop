package ictgradschool.industry.lab_oop.ex07;

public class ExerciseSeven {
    public void start() {
        String colours, first, second, third;
        int position1, position2, position3, length;
        colours = "redorangeyellow";
        //range
        first = colours.substring(4, 9);
        //redo
        second = colours.substring(0, 4);
        //row
        third = colours.charAt(0) + colours.substring(13);
        //3
        length = third.length();
        //ROW
        third = third.toUpperCase();
        //-1
        position1 = colours.indexOf('A');
        //10
        position2 = colours.indexOf("el");
        //3
        position3 = colours.indexOf("or");
        System.out.println("first: " + first);
        System.out.println("second: " + second);
        System.out.println("third: " + third);
        System.out.println("length: " + length);
        System.out.println("position1: " + position1);
        System.out.println("position2: " + position2);
        System.out.println("position3: " + position3);
    }

    public static void main(String[] args) {
        ExerciseSeven e=new ExerciseSeven();
        e.start();
    }
}
