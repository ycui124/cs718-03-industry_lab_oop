package ictgradschool.industry.lab_oop.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {
        /*
        Lower bound? 19
        Upper bound? 42
        3 randomly generated numbers: 29, 32 and 23
        Smallest number is 23
         */
//        System.out.print("Lower bound? ");
//        String lower = Keyboard.readInput();
//        System.out.print("Upper bound? ");
//        String upper = Keyboard.readInput();
//        int intLower = Integer.parseInt(lower);
//        int intUpper = Integer.parseInt(upper);
        int lower = getNums("Lower");
        int upper = getNums("Upper");
        int num = 0;
        int[] arr = new int[3];
        for (int i = 0; i < 3; i++) {
            num = (int) (lower + Math.random() * (upper - lower));
            arr[i] = num;
        }
        System.out.println("3 randomly generated numbers: " + arr[0] + ", " + arr[1] + " and " + arr[2]);
        int midSmall = Math.min(arr[0], arr[1]);
        int small = Math.min(arr[2], midSmall);
        System.out.println("Smallest number is " + small);
    }

    private int getNums(String prompt) {
        System.out.print(prompt + " bound? ");
        String strBound = Keyboard.readInput();
        int bound = 0;
        if (!(strBound.equals(""))){
            bound=Integer.parseInt(strBound);
        }

        return bound;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
