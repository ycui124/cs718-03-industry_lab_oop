package ictgradschool.industry.lab_oop.ex08;

import ictgradschool.Keyboard;

public class ExerciseEight {
    /*
    public - start() {
    double radius-
    System.out.println("\"Volume of a Sphere-"");
    System.out.println("Enter the radius: -);
    radius = Integer.parseInt(Keyboard.readInput--);
    int- volume =- 4 / 3 * Math.PI * Math.pow(radius,2-);
    System.out.println("Volume: ",- volume);
    }
     */
    public void start() {
        double radius;
        System.out.println("\"Volume of a Sphere\"");
        System.out.print("Enter the radius: ");
        radius = Double.parseDouble(Keyboard.readInput());
        double volume = 4 * Math.PI * Math.pow(radius,3)/ 3;
        System.out.println("Volume: "+ volume);
    }

    public static void main(String[] args) {
        ExerciseEight e=new ExerciseEight();
        e.start();
    }
}
