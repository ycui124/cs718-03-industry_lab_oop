package ictgradschool.industry.lab_oop.ex09;

public class Deodorant {

    private String brand;
    private String fragrance;
    private boolean rollOn;
    private double price;

    public Deodorant(String brand, String fragrance,
                     boolean rollOn, double price) {

        this.brand = brand;
        this.fragrance = fragrance;
        this.rollOn = rollOn;
        this.price = price;
    }

    public String toString() {
        String info = brand + " " + fragrance;
        if (rollOn) {
            info = info + " Roll-On";
        } else {
            info = info + " Spray";
        }
        info += " Deodorant, \n$" + price;
        return info;
    }

    // TODO Implement all methods below this line.

    public double getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }

    public boolean isRollOn() {

        return rollOn;
    }

    public String getFragrance() {
        return fragrance;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setFragrance(String fragrance) {
        this.fragrance = fragrance;
    }

    public boolean isMoreExpensiveThan(Deodorant other) {
//        Deodorant deo1 = new Deodorant("Gentle", "Baby Powder", true, 4.99);
//        Deodorant deo2 = new Deodorant("Spring", "Blossom", false, 3.99);
        if (price>other.price){
            return true;
        }
        return false;
    }
}