package ictgradschool.industry.lab_oop.ex03;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a sentence, then prints out the sentence with a random character
 * missing. The program is to be written so that each task is in a separate method. See the comments below for the
 * different methods you have to write.
 */
public class ExerciseThree {

    private void start() {

//Printing the prompt and reading the input from the user
        String sentence = getSentenceFromUser();
//Generating an appropriate random number
        int randomPosition = getRandomPosition(sentence);
//Printing the character that is going to be removed
        printCharacterToBeRemoved(sentence, randomPosition);
//Generating a new sentence by removing the character at the random position
        String changedSentence = removeCharacter(sentence, randomPosition);
//Printing the new sentence
        printNewSentence(changedSentence);
    }

    /**
     * Gets a sentence from the user.
     *
     * @return
     */
    private String getSentenceFromUser() {

        // TODO Prompt the user to enter a sentence, then get their input.
        System.out.print("Enter a sentence: ");
        String sentence = Keyboard.readInput();
        if (!(sentence.equals(""))) {
            return sentence;
        }
        return null;
    }

    /**
     * Gets an int corresponding to a random position in the sentence.
     */
    private int getRandomPosition(String sentence) {

        // TODO Use a combination of Math.random() and sentence.length() to get the desired result.
        int length = sentence.length();
//        System.out.println("--------length------" + length);
        int position;
        if (length >= 0) {
            position = (int) (Math.random() * length);
//            System.out.println("--------posi------" + position);
            return position;
        }

        return -1;
    }

    /**
     * Prints a message stating the character to be removed, and its position.
     */
    private void printCharacterToBeRemoved(String sentence, int position) {

        // TODO Implement this method
        String remove = sentence.substring(position, position + 1);
        System.out.println("Removing \"" + remove + "\" from position " + position);

    }

    /**
     * Removes a character from the given sentence, and returns the new sentence.
     */
    private String removeCharacter(String sentence, int position) {

        // TODO Implement this method
        String newSentence;
        if (!(sentence.equals(""))){
            newSentence = sentence.substring(0, position) +
                    (sentence.substring(position + 1, sentence.length()));
            return newSentence;
        }
        return null;

    }

    /**
     * Prints a message which shows the new sentence after the removal has occured.
     */
    private void printNewSentence(String changedSentence) {

        // TODO Implement this method
        System.out.println("New sentence is: "+changedSentence);
//        System.out.println("-----newLength----"+changedSentence.length());
    }

    public static void main(String[] args) {
        ExerciseThree ex = new ExerciseThree();
        ex.start();
    }
}
